# Change Log
- Changes before version 0.2.1 not documented (see commit history)
- Project follows [Semantic Versioning](http://semver.org/)

## Version 0.2.4
- Fix wrong version changes.

## Version 0.2.3
- Added 'disabled buttons' feature.

## Version 0.2.2
- Readme documentation updated.

## Version 0.2.1
- Added millions and thousands abbreviation props.
- Readme updated.
