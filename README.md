# vue-like-dislike-buttons

[![Npm version](https://img.shields.io/npm/v/vue-like-dislike-buttons.svg?maxAge=2592000)](https://www.npmjs.com/package/vue-like-dislike-buttons)

## Installation

```
npm install vue-like-dislike-buttons
```

or

```
yarn add vue-like-dislike-buttons
```

## Usage

```HTML
<vue-like-dislike-buttons :likes="876547" :dislikes="4567" likeChecked />
```

```javascript
import VueLikeDislikeButtons from 'vue-like-dislike-buttons'

export default {
  components: {
    VueLikeDislikeButtons
  }
}
```

And styles:

```javascript
import "vue-like-dislike-buttons/src/assets/scss/main.scss"
```

SCSS variables:

```scss
$color-unchecked: #a7a7a7 !default;
$color-hover: darken($color-unchecked, 15%) !default;
$checked-color: darken($color-unchecked, 25%) !default;
@import "~vue-like-dislike-buttons/src/assets/scss/main.scss"
```

## API

### like-dislike-buttons 

#### props 

- `dislike-btn-title` ***String*** (*optional*) `default: 'I don't like it'` 

- `dislike-checked` ***Boolean*** (*optional*) `default: false` 

- `dislikes` ***Number*** (*optional*) 

- `is-disabled` ***Boolean*** (*optional*) `default: false` 

- `like-btn-title` ***String*** (*optional*) `default: 'I like it'` 

- `like-checked` ***Boolean*** (*optional*) `default: false` 

- `likes` ***Number*** (*optional*) 

- `millions-abbreviation` ***String*** (*optional*) `default: 'M'` 

- `only-like` ***Boolean*** (*optional*) `default: false` 

- `thousands-abbreviation` ***String*** (*optional*) `default: 'K'` 

#### methods 

- `computeNumber(value)` 

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your tests

```
yarn test
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Update the API section of README.md with generated documentation

```
yarn doc:build
```

### Run style guide dev server

```
yarn styleguide
```

### Generate a static HTML style guide

```
yarn styleguide:build
```

### Update project version

```
yarn version
```
